import { useState } from 'react';
import { NavLink as ReactLink} from 'react-router-dom';
import {
  Collapse, DropdownItem, DropdownMenu, DropdownToggle, Nav, Navbar, NavbarBrand, NavbarText, NavbarToggler, NavItem,
  NavLink,
  UncontrolledDropdown
} from 'reactstrap';

function CustomNavbar(args) {

  const [isOpen,setIsOpen]  = useState(false)

  return (
    <div>
      <Navbar color='dark' expand="md" fixed=''>
        <NavbarBrand tag={ReactLink} to="/">My Blogs</NavbarBrand>
        <NavbarToggler onClick={()=>setIsOpen(!isOpen)} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="me-auto" navbar>
            <NavItem>
              <NavLink tag={ReactLink} to="/about" >About</NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={ReactLink} to="/login">
                Login
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={ReactLink} to="/signup/">
                Sign Up
              </NavLink>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                More
              </DropdownToggle>
              <DropdownMenu end>
                <DropdownItem tag={ReactLink} to="/services" >Services</DropdownItem>
                <DropdownItem>Contant Us</DropdownItem>
                <DropdownItem divider />
                <DropdownItem>Youtube</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
          <NavbarText>Youtube</NavbarText>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default CustomNavbar;