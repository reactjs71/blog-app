
import {
  Navbar, NavbarBrand
} from 'reactstrap';

function CustomFooter(args) {


  return (
    <div>
      <Navbar color='dark' expand="md" fixed='bottom' >
        <NavbarBrand>ReactDemoByNil - ©2022</NavbarBrand>
      </Navbar>
    </div>
  );
}

export default CustomFooter;