import { useState } from "react";
import { toast } from "react-toastify";
import {
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Card,
  CardBody,
  CardHeader,
  Container,
  Button,
  FormFeedback,
} from "reactstrap";
import Base from "../components/Base";
import { singUp } from "../services/user-service";

const Signup = () => {
  // Object to store form data
  const [data, setData] = useState({
    name: "",
    email: "",
    password: "",
    about: "",
  });

  const [error, setError] = useState({
    errors: {},
    isError: false,
  });

  // Function to handle change event
  const handleChange = (event, property) => {
    setData({ ...data, [property]: event.target.value });
  };

  // Function to submit form
  const submitForm = (event) => {
    event.preventDefault();

    // if (error.isError) {
    //   toast.error(
    //     error.errors.response.data +
    //       " The form is not valid. Please check input data.!!"
    //   );
    //   return;
    // }

    console.log("Submitted Data");
    console.log(data);

    singUp(data)
      .then((resp) => {
        console.log("Api response");
        console.log(resp);
        toast.success("User is registerted successfully");
        setData({
          name: "",
          email: "",
          password: "",
          about: "",
        });
      })
      .catch((error) => {
        console.log("Api Error response");
        console.log(error);
        setError({
          errors: error,
          isError: true,
        });
      });
  };

  // Function to reset form
  const resetForm = () => {
    setData({
      name: "",
      email: "",
      password: "",
      about: "",
    });
  };

  // Function to print data after changing value of field
  // useEffect(() => {
  //   console.log(data);
  // }, [data]);

  return (
    <Base>
      <div>
        <Container>
          <Row className="mt-4">
            <Col sm={{ size: 6, offset: 3 }}>
              <Card color="dark">
                <CardHeader>
                  <h1>Fill information to register!!!</h1>
                </CardHeader>
                <CardBody>
                  <Form onSubmit={(e) => submitForm(e)}>
                    {JSON.stringify(data)}
                    <FormGroup>
                      <Label for="name">Name</Label>
                      <Input
                        id="name"
                        placeholder="Enter here"
                        type="text"
                        onChange={(e) => handleChange(e, "name")}
                        value={data.name}
                        invalid={
                          error.errors?.response?.data?.name ? true : false
                        }
                      />
                      <FormFeedback>
                        {error.errors?.response?.data?.name}
                      </FormFeedback>
                    </FormGroup>
                    <FormGroup>
                      <Label for="email">Email</Label>
                      <Input
                        id="email"
                        placeholder="Enter here"
                        type="email"
                        onChange={(e) => handleChange(e, "email")}
                        value={data.email}
                        invalid={
                          error.errors?.response?.data?.email ? true : false
                        }
                      />
                      <FormFeedback>
                        {error.errors?.response?.data?.email}
                      </FormFeedback>
                    </FormGroup>
                    <FormGroup>
                      <Label for="password">Password</Label>
                      <Input
                        id="password"
                        placeholder="Enter here"
                        type="password"
                        onChange={(e) => handleChange(e, "password")}
                        value={data.password}
                        invalid={
                          error.errors?.response?.data?.password ? true : false
                        }
                      />
                      <FormFeedback>
                        {error.errors?.response?.data?.password}
                      </FormFeedback>
                    </FormGroup>
                    <FormGroup>
                      <Label for="about">About</Label>
                      <Input
                        id="about"
                        placeholder="Enter here"
                        type="textarea"
                        style={{ height: "150px" }}
                        onChange={(e) => handleChange(e, "about")}
                        value={data.about}
                        invalid={
                          error.errors?.response?.data?.about ? true : false
                        }
                      />
                      <FormFeedback>
                        {error.errors?.response?.data?.about}
                      </FormFeedback>
                    </FormGroup>
                    <Container className="text-center">
                      <Button color="primary">Register</Button>
                      <Button
                        color="light"
                        className="ms-2"
                        type="reset"
                        onClick={resetForm}
                      >
                        Reset
                      </Button>
                    </Container>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </Base>
  );
};
export default Signup;
